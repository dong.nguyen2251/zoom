import 'package:flutter/material.dart';
import 'package:mymid/utils/colors.dart';

import '../widgets/home_meeting_button.dart';
import '../widgets/others_meeting_button.dart';

class MeetingScreen extends StatelessWidget {
  const MeetingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
        backgroundColor: Colors.black,
        elevation: 0,
        title: const Text('Gặp gỡ và trò chuyện'),
        centerTitle: true,
        actions: []),
      
      body: Column(
      children: [
       
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              HomeMeetingButton(
                onPressed: (){},
                text:'Cuộc họp mới',
                icon: Icons.videocam,
                
              ),
              OtherMeetingButton(
                onPressed: (){}, 
                icon: Icons.add_box_rounded, 
                text: 'Tham gia'
                ),
              OtherMeetingButton(
                onPressed: (){}, 
                icon: Icons.calendar_today, 
                text: 'Lên lịch'
                ),
              OtherMeetingButton(
                onPressed: (){}, 
                icon: Icons.arrow_upward_rounded, 
                text: 'Chia sẻ màn hình'
                ),  
            ],
          ),
        ),
        const Expanded(
          child: Center(
            child: Text('Tìm người và bắt đầu cuộc trò chuyện!',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18),),
          ),
          ),
      ],
    ),
    );
  }
}
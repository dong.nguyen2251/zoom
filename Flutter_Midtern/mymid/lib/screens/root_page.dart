import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mymid/screens/account.dart';
import 'package:mymid/screens/constact_home_screen.dart';
import 'package:mymid/screens/history_meeting_screen.dart';
import 'package:mymid/screens/meeting.dart';
import 'package:mymid/utils/colors.dart';
import 'package:mymid/widgets/home_meeting_button.dart';
import 'package:mymid/widgets/others_meeting_button.dart';

class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);

  @override
  State<RootPage> createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int _page = 0;
  onPageChanged(int page){
    setState(() {
      _page = page;
    });
  }
  List<Widget> pages = [
    const MeetingScreen(),
    const HistoryMeetingScreen(),
    const ContactScreen(),
    const AccountScreen()
    //const Text('Khác')
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
        body: pages[_page],
        bottomNavigationBar: BottomNavigationBar(
        backgroundColor: footerColor,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        onTap: onPageChanged,
        currentIndex: _page,
        type: BottomNavigationBarType.fixed,
        unselectedFontSize: 14,
        
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.comment_bank,),
            label: 'Gặp gỡ và trò chuyện',
          ),

          BottomNavigationBarItem(
            
            icon: Icon(Icons.lock_clock,),
            label: 'Cuộc họp',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.person_outline),
            label: 'Liên lạc',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.settings_outlined,),
            label: 'Khác',
          ),  
        ]),
    );
  }
}


import 'package:flutter/material.dart';
class Channel extends StatelessWidget {
  const Channel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          'Không có kênh nào để hiển thị',
          style: TextStyle(
            color: Colors.grey,
            fontSize: 18),)),
    );
  }
}
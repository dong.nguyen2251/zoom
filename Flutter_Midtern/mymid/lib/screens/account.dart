import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:mymid/models/user_model.dart';
import 'package:mymid/screens/login.dart';
import 'package:mymid/utils/colors.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    FirebaseFirestore.instance
    .collection("users")
    .doc(user!.uid)
    .get()
    .then((value) => {
      this.loggedInUser = UserModel.fromMap(value.data()),
      setState((){
        
      }),
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
        backgroundColor: Colors.black,
        elevation: 0,
        title: const Text('Khác'),
        centerTitle: true,
        actions: []),
        body: ListView(
          children: [
           
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                
                
                 Padding(
                   padding: const EdgeInsets.fromLTRB(15, 50, 15,15),
                   child: Row(
                     children: [
                       
                       Icon(
                         Icons.account_box_outlined,
                         color: Colors.blueAccent,),
                       
                       Text(
                         "${loggedInUser.firstName} ${loggedInUser.secondName}",
                         style: TextStyle(
                           color: Colors.white,
                           fontWeight: FontWeight.w500,
                           fontSize: 20),
                       ),
                     
                     ],
                       
                     
                   ),
                 ),
                 Padding(
                   padding: const EdgeInsets.fromLTRB(15, 5, 10, 0),
                   child: Row(
                     children: [
                       Icon(
                         Icons.email_outlined,
                         color: Colors.blueAccent,),
                       Text('${loggedInUser.email}',
                       style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 20),),
                     ],
                   ),
                 ),
                  
                   
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 25, 0, 0),
              child: Text(
                'CÁC TÍNH NĂNG ĐÃ THÊM',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.grey),
              ),),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 25, 0, 0),
              child: Text(
                'CÀI ĐẶT',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.grey),
              ),), 
              Padding(
              padding: const EdgeInsets.fromLTRB(30, 25, 0, 0),
              child: Text(
                'KHÁC',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.grey),
              ),),
            Padding(
              padding: const EdgeInsets.all(10),
              child: ActionChip(
                      
                      backgroundColor: Colors.black,
                      label: Text('Đăng xuất',
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.redAccent),), 
                      onPressed: (){
                        loggout(context);
                      }),
            ),
          ],
          
            )
            );
    
    
  }

  Future<void> loggout(BuildContext context) async{
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=> LoginScreen()));
  }
}
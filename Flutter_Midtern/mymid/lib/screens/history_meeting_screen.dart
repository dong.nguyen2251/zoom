import 'package:flutter/material.dart';
import 'package:mymid/screens/video_screen.dart';

import '../utils/colors.dart';

class HistoryMeetingScreen extends StatelessWidget {
  const HistoryMeetingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
        backgroundColor: Colors.black,
        elevation: 0,
        title: const Text('Cuộc họp'),
        centerTitle: true,
        actions: []),
       body: Column(
         children: [
           Container(
            child:Center(
            child:Text(
           'ID Cuộc họp Cá nhân',
           style: TextStyle(
             
             fontSize: 15),
             textAlign: TextAlign.center,),
             ),
             
           ),
            Container(
            child:Center(
            child:Text(
           '604 643 6349',
           style: TextStyle(
             fontWeight: FontWeight.bold,
             fontSize: 25),
             textAlign: TextAlign.center,),
             ),
             
           ),
           Row(
             mainAxisAlignment: MainAxisAlignment.center,
             children: [
                Padding( 
                  padding: const EdgeInsets.all(20),
                  child: GestureDetector(
                    child: Text(
                      'Bắt đầu',
                      style: TextStyle(
                        fontWeight:FontWeight.bold,fontSize: 15,color: Colors.white),
                        ),
                    onTap: (){
                      Navigator.push(context,MaterialPageRoute(builder: (context)=>VideoScreen()));
                    },    
                  ),
                ),
                Padding( 
                  padding: const EdgeInsets.all(20),
                  child: GestureDetector(
                    child: Text(
                      'Gửi lời mời',
                      style: TextStyle(
                        fontWeight:FontWeight.bold,fontSize: 15,color: Colors.white),
                        ),
                    onTap: (){},    
                  ),
                ),
                Padding( 
                  padding: const EdgeInsets.all(20),
                  child: GestureDetector(
                    child: Text(
                      'Chỉnh sửa',
                      style: TextStyle(
                        fontWeight:FontWeight.bold,fontSize: 15,color: Colors.white),
                        ),
                    onTap: (){},    
                  ),
                ),
             ],
           ),
           const Expanded(
            child: Center(
            child: Text('Không có Cuộc họp Sắp diễn ra!',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25),
              ),
            
              
          ),

          ),
          
         ],
        
    ),
    );
  }
}
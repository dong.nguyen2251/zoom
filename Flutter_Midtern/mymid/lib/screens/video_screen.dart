import 'package:flutter/material.dart';
import 'package:mymid/screens/history_meeting_screen.dart';

class VideoScreen extends StatefulWidget {
  const VideoScreen({Key? key}) : super(key: key);

  @override
  State<VideoScreen> createState() => _VideoScreenState();
}

class _VideoScreenState extends State<VideoScreen> {
  int _page = 0;
  onPageChanged(int page){
    setState(() {
      _page = page;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        leading: Icon(Icons.speaker_outlined),
        title: const Text('Zoom',
        style: TextStyle(color: Colors.white),
        textAlign: TextAlign.center,),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0,8,8,8),
            
            child: TextButton(
              
              onPressed: (){
                Navigator.pop(context,MaterialPageRoute(builder: (context)=>HistoryMeetingScreen()));
              },
              child:Text('Kết thúc',
              
              style: TextStyle(
                backgroundColor: Colors.redAccent,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 20,
                ),) ),
          ),

        ]),
        body: Center(child: Image.asset('assets/images/zoom.jpg')),
        bottomNavigationBar: BottomNavigationBar(
        onTap: onPageChanged,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,

        type: BottomNavigationBarType.fixed,
        unselectedFontSize: 14,
          items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.mic_none_outlined,),
            label: 'Tắt tiếng',
          ),

          BottomNavigationBarItem(
            
            icon: Icon(Icons.camera_outdoor_outlined),
            label: 'Cuộc họp',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.share_outlined),
            label: 'Chia sẻ màn hình',
            backgroundColor: Colors.green
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.account_box_outlined,),
            label: 'Người tham gia',
          ),  

          BottomNavigationBarItem(
            icon: Icon(Icons.more_horiz_outlined),
            label: 'Khác',
          ), 

        ]),
    );
  }
}
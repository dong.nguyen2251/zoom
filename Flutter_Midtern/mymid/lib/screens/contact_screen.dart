import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:mymid/widgets/search_form.dart';
import 'package:mymid/data/list_friend_json.dart';
class ConTactScreen extends StatelessWidget {
  const ConTactScreen(
    {Key? key,
    }
    ) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Column(
            children: [
              const SearchForm(),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10,20,0,0),
            child: Text(
              'Danh bạ của tôi',
              style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
            
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 10, 0, 0),
            child: DropdownButtonHideUnderline(
              child: DropdownButton2(
                hint: Text('Đã gắn sao'),
                items: [],

                icon: const Icon(Icons.arrow_forward_ios_outlined),
                iconEnabledColor: Colors.grey,
                iconDisabledColor: Colors.grey,
              ),
              ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
            child: DropdownButtonHideUnderline(
              
              child: DropdownButton2(
                isExpanded: true,
                hint: Text('Danh sách bạn'),
                items: _addDividersAfterItems(friends),
                customItemsIndexes: _getDividersIndexes(),
                customItemsHeight: 4,
                value: selectedValue,
                onChanged: (value) {
                  setState() {
                selectedValue = value as String;
            };
          },
                buttonHeight: 40,
                buttonWidth: 140,
                itemHeight: 40,
                itemPadding: const EdgeInsets.symmetric(horizontal: 8.0),

                icon: const Icon(Icons.arrow_forward_ios_outlined),
                iconEnabledColor: Colors.blue ,
                iconDisabledColor: Colors.grey,
              ),
              ),
          ),
            ],
          ),
          
        
      );
    
  }
}
List<DropdownMenuItem<String>> _addDividersAfterItems(List<String> friends) {
  List<DropdownMenuItem<String>> _listFriends = [];
  for (var friend in friends) {
    _listFriends.addAll(
      [
        DropdownMenuItem<String>(
          value: friend,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              friend,
              style: const TextStyle(
                fontSize: 14,
              ),
            ),
          ),
        ),
        //If it's last item, we will not add Divider after it.
        if (friend != friends.last)
          const DropdownMenuItem<String>(
            enabled: false,
            child: Divider(),
          ),
      ],
    );
  }
  return _listFriends;
}

List<int> _getDividersIndexes() {
  List<int> _dividersIndexes = [];
  for (var i = 0; i < (friends.length * 2) - 1; i++) {
    //Dividers indexes will be the odd indexes
    if (i.isOdd) {
      _dividersIndexes.add(i);
    }
  }
  return _dividersIndexes;
}
import 'package:flutter/material.dart';
import 'package:mymid/screens/channel.dart';
import 'package:mymid/screens/contact_screen.dart';
import 'package:mymid/widgets/search_form.dart';

class ContactScreen extends StatelessWidget {
  const ContactScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
       appBar: AppBar(
        
        backgroundColor: Colors.black,
        elevation: 0,
        bottom: TabBar(
          tabs: [
            Tab(child: Text('Liên lạc'),),
            Tab(child: Text('Kênh'),)
          ]),
          ),
        body: TabBarView(
          children: [
            const ConTactScreen(),
            const Channel()
           
          ],
        ),
    ),
    );
  }
}
import 'package:flutter/material.dart';


const kNormalText = TextStyle(
  fontSize: 12,
  color: Colors.white,
);

const kUnderlinedText = TextStyle(
  fontSize: 12,
  color: Colors.white,
  decoration: TextDecoration.underline,
  fontWeight: FontWeight.w500,
);
const Color primaryColor = Color(0xFFF67952);
const Color bgColor = Color.fromARGB(255, 240, 244, 242);
const Color bgBody = Color.fromARGB(0, 130, 159, 233);
const double defaultPadding = 16.0;
const double defaultBorderRadius = 12.0;  

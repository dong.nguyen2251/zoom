import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mymid/screens/login.dart';
import 'package:splashscreen/splashscreen.dart';

class SplashScreenPage extends StatefulWidget {
  @override 
  _SplashScreenPageState createState() => _SplashScreenPageState();

}
class _SplashScreenPageState extends State<SplashScreenPage>{
   int loadingPercent = 0;

  Future<Widget> loadFromFuture() async {
    // <fetch data from server. ex. login>
    while(this.loadingPercent < 100)  {
      this.setState(() {
        this.loadingPercent++;
        print("Percent: " + this.loadingPercent.toString());
      });
      // Delay 100 millisecond.
      await Future.delayed(const Duration(milliseconds : 100));
    }
    // Show Main Screen (After Splash Screen)
    return Future.value(LoginScreen());
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
       
        navigateAfterFuture: loadFromFuture(),
        
        
        title: Text(
          'Welcome to Zoom',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 50.0,
              color: Colors.white
          ),
          ),
        
        
        backgroundColor: Colors.blueAccent,
        styleTextUnderTheLoader: TextStyle(),
        photoSize: 100.0,
        onClick: () => print(""),
        loadingText: Text("Loading " + this.loadingPercent.toString() +" %"),
        loaderColor: Colors.white
    );
  }

}

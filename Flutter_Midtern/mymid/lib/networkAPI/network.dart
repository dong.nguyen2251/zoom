import 'dart:convert';
import 'package:flutter/foundation.dart';

import 'package:mymid/models/zoom.dart';
import 'package:http/http.dart' as http;

List<Zoom> parseZoom(String responseBody){
  var list = json.decode(responseBody) as List<dynamic>;
  List<Zoom>? zoom = list.map((model) => Zoom.fromJson(model)).toList();
  return zoom;
}

Future<List<Zoom>> fetchZoom() async{
  String url = 'https://627a64c74a5ef80e2c196c05.mockapi.io/api/listfriend';
  Uri uri = Uri.parse(url);
  final response = await http.get(uri);
  if(response.statusCode == 200)
    {
      return compute(parseZoom, response.body);
    } else {
    throw Exception('Request API Error');
  }
}
